@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pb-4 banner">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Banner Iklan</h1>
            <small>Iklan produk dengan banner</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary py-1 px-3">
                    <small>Beri judul jika ada</small>
                    <div class="float-right">
                        <a href="{{ route('admin.form', Crypt::encrypt('1')) }}" class="badge badge-primary"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                </div>
                <div class="card-body bg-white table-responsive px-0 pt-0">
                    <table class="table table-striped text-center table-content-middle" id="dataTable2">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>URL</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($banner as $val)
                            <tr>
                                <td>
                                    <a href="{{ asset($val->thumbnail) }}" type="_blank">
                                        <img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data">
                                    </a>
                                </td>
                                <td>{{ substr($val->title, 0, 30) . '...' }}</td>
                                <td>
                                    <a href="{{ route('admin.form.edit', [Crypt::encrypt('1'), Crypt::encrypt($val->id)]) }}" class="badge badge-warning"><i class="fa fa-pen"></i> Edit</a>
                                    <a href="{{ route('admin.form.delete', [Crypt::encrypt('1'), Crypt::encrypt($val->id)]) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Terpopuler -->
<div class="container-fluid border-top pb-4 terpopuler" style="background-color: #F5F5F5;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Terpopuler</h1>
            <small>Berikan maximal tiga (3) produk yang paling populer</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary py-1 px-3">
                    <small>Beri judul jika ada</small>
                    <?php if ($populer->count() < 3) : ?>
                        <div class="float-right">
                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#modal-terpopuler"><i class="fa fa-plus"></i> Tambah</a>
                        </div>
                    <?php endif ?>
                </div>
                <div class="card-body bg-white table-responsive px-0 pt-0">
                    <table class="table table-striped text-center table-content-middle" id="dataTable2">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($populer as $val)
                            <tr>
                                <td>
                                    <img src="{{ asset($val->produk->thumbnail) }}" alt="thumbnail" class="img-banner-data">
                                </td>
                                <td>{{ $val->produk->nama }}</td>
                                <td><a href="{{ route('admin.produk.populer.delete', Crypt::encrypt($val->id)) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Galeri -->
<div class="container-fluid border-top pb-4 galeri">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Galeri</h1>
            <small>Berikan bentuk dan style produk dalam galeri</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary py-1 px-3">
                    <small>Beri judul jika ada</small>
                    <div class="float-right">
                        <a href="{{ route('admin.form', Crypt::encrypt('3')) }}" class="badge badge-primary"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                </div>
                <div class="card-body bg-white table-responsive px-0 pt-0">
                    <table class="table table-striped text-center table-content-middle" id="dataTable2">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($galeri as $val)
                            <tr>
                                <td>
                                    <img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data">
                                </td>
                                <td>{{ $val->title }}</td>
                                <td>{{ $val->deskripsi }}</td>
                                <td>
                                    <a href="{{ route('admin.form.edit', [Crypt::encrypt('3'), Crypt::encrypt($val->id)]) }}" class="badge badge-warning"><i class="fa fa-pen"></i> Edit</a>
                                    <a href="{{ route('admin.form.delete', [Crypt::encrypt('3'), Crypt::encrypt($val->id)]) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<!-- Terpopuler -->
<div class="modal fade" id="modal-terpopuler">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title small"><strong>Tambah Terpopuler</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <table class="table table-striped table-content-middle">
                    <thead>
                        <tr>
                            <th>Thumbnail</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($terpopuler as $val)
                        <tr>
                            <td><img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data"></td>
                            <td>{{ $val->nama }}</td>
                            <td><a href="{{ route('admin.produk.populer', Crypt::encrypt($val->id)) }}" class="badge badge-primary"><i class="fa fa-plus"></i> Tambah</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer justify-content-between bg-primary">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection