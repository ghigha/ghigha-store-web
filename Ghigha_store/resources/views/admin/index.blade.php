@extends('layouts.admin_template')
@section('content')
<div class="container-fluid">
    <h1 class="my-4 mb-0">Dashboard</h1>
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header bg-primary"></div>
                <div class="card-body table-responsive p-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Diskon</th>
                                <th>Harga Diskon</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produk as $val)
                            <tr>
                                <td><img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data"></td>
                                <td>{{ $val->nama }}</td>
                                <td>Rp {{ number_format($val->harga, 0, ',', '.') }}</td>
                                <td>{{ $val->diskon }}%</td>
                                <td>Rp {{ number_format(round($val->harga - ($val->diskon / 100 * $val->harga)), 0, ',', '.') }}</td>
                                <td>{{ substr($val->deskripsi, 3, 20) . '...' }}</td>
                                <td>
                                    <a href="{{ route('admin.produk.detail', Crypt::encrypt($val->id)) }}" class="badge badge-primary"><i class="fa fa-eye"></i> Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection