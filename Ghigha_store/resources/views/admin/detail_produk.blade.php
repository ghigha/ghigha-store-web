@extends('layouts.admin_template')
@section('content')
<div class="juna-container produk-detail-page pt-3">
    <div class="text-right">
        <a href="{{ route('admin.form.produk.form_edit', Crypt::encrypt($produk->id)) }}" class="btn btn-warning"><i class="fa fa-pen"></i> Edit</a>
    </div>
    <div class="shadow-lg pb-4">
        <div class="juna-container py-3 mt-4 bg-light text-center text-black">
            <h5>{{ $produk->promosi }}</h5>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-6 px-0">
                    <img src="{{ asset($produk->thumbnail) }}" alt="Produk" id="img-first" data-toggle="modal" data-target="#modal-img-detail">
                    <div class="owl-carousel owl-theme img-produk-small">
                        <div class="item">
                            <img src="{{ asset($produk->thumbnail) }}" alt="Produk-more">
                        </div>
                        @foreach($img_more as $val)
                        <div class="item">
                            <a href="{{ route('admin.form.img_more.del', [Crypt::encrypt($val->id),Crypt::encrypt($produk->id)]) }}" class="btn-del-img-more text-danger" style="position: absolute; right: 0;top:0; z-index:1"><i class="fa fa-trash"></i></a>
                            <img src="{{ asset($val->thumbnail) }}" alt="Produk-more">
                        </div>
                        @endforeach
                    </div>
                    <div class="mt-4 mb-4 text-center">
                        <a href="{{ route('admin.form.img_more', Crypt::encrypt($produk->id)) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Image</a>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="produk-detail">
                        <small>{{ $produk->kategori->kategori }}</small>
                        <h1>{{ $produk->nama }}</h1>
                        <h6>Nyaman untuk dipakai</h6>
                        <div class="mt-4">
                            <span class="badge badge-danger">Diskon {{ $produk->diskon }}%</span>
                            <h6 class="coret-harga mb-0">Rp {{ number_format($produk->harga, 0, ',', '.') }}</h6>
                            <h1 class="text-success">Rp {{ number_format(round($produk->harga - ($produk->diskon/100*$produk->harga)), 0, ',', '.') }}</h1>
                        </div>
                        <div class="deskripsi mt-4 pt-4">
                            <div class="font-weight-bold mb-3">
                                Deskripsi
                            </div>
                            <p>
                                {!! $produk->deskripsi !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center testimonial bg-light">
            <div class="juna-container px-1">
                <div class="h3 text-black" style="font-family: sans-serif;">
                    <small>TESTIMONI</small>
                </div>
                <div class="h5">
                    <small>Percaya pada mereka yang sudah membeli produk kami</small>
                </div>
                <div class="owl-carousel owl-theme">
                    @foreach($testimoni as $val)
                    <div class="item">
                        <img src="{{ asset($val->thumbnail) }}" alt="Produk-more" style="height: 100%; width:auto; margin-left:50%; transform:translate(-50%, 0);">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Paking produk -->
        <div class="video" data-jml="{{ $video->count() }}">
            <div class="juna-container text-center">
                <div class="h3 text-black" style="font-family: sans-serif;">
                    <small>VIDEO PRODUK</small>
                </div>
                <div class="h5">
                    <small>Ghigha Store</small>
                </div>
                <div class="owl-carousel owl-theme" style="margin-top: 3rem; margin-bottom: 3rem">
                    @foreach($video as $val)
                    <div class="item">
                        <video class="w-100" controls>
                            <source src="{{ asset($val->thumbnail) }}" type="video/mp4">
                        </video>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection