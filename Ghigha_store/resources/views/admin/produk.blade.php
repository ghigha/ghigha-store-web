@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pb-4 produk">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Produk</h1>
            <small>Tambahkan produk anda dengan jalas</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary">
                    <div class="float-right">
                        <a href="{{ route('admin.form.produk.form_produk') }}" class="badge badge-primary"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                </div>
                <div class="card-body bg-white table-responsive px-0 pt-1">
                    <table class="table table-striped table-content-middle" id="dataTable2">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Diskon</th>
                                <th>Harga Diskon</th>
                                <th>Kategori</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produk as $val)
                            <tr>
                                <td><img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data"></td>
                                <td>{{ $val->nama }}</td>
                                <td>Rp {{ number_format($val->harga, 0, ',', '.') }}</td>
                                <td>{{ $val->diskon }}%</td>
                                <td>Rp {{ number_format(round($val->harga - ($val->diskon / 100 * $val->harga)), 0, ',', '.') }}</td>
                                <td>{{ $val->kategori->kategori }}</td>
                                <td>{{ substr($val->deskripsi, 3, 20) . '...' }}</td>
                                <td>
                                    <a href="{{ route('admin.produk.detail', Crypt::encrypt($val->id)) }}" class="badge badge-primary"><i class="fa fa-eye"></i> Detail</a>
                                    <a href="{{ route('admin.form.produk.delete', Crypt::encrypt($val->id)) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection