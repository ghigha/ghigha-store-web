@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pb-4 banner">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Setting</h1>
            <small>Semua Penting, data tambahan untuk kebutuhan</small>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card text-white mt-4">
                        <div class="card-header bg-primary py-1 px-3">
                            <small>No Telepon</small>
                        </div>
                        <div class="card-body bg-white">
                            <form action="{{ route('admin.setting.store_nohp') }}" method="post">
                                @csrf
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary" type="button"><i class="fab fa-whatsapp"></i></button>
                                    </div>
                                    <input class="form-control" type="number" name="no_telepon" placeholder="No Telepon" value="{{ $no_hp->no_telepon }}" aria-label="no_hp" aria-describedby="basic-addon2" />
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-warning" type="button"><i class="fas fa-pen"></i></button>
                                    </div>
                                </div>
                                <small class="text-dark">Masukan nomor dan click icon untuk mengubah</small>
                            </form>
                        </div>
                        <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                            <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                        </div>
                    </div>
                    <!-- Kategori -->
                    <div class="card text-white mt-4">
                        <div class="card-header bg-primary py-1 px-3">
                            <small>Kategori</small>
                            <div class="float-right">
                                <a href="#" class="badge badge-primary insert-kategori" data-toggle="modal" data-target="#modal-kategori"><i class="fa fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                        <div class="card-body bg-white table-responsive px-0 pt-0">
                            <table class="table table-striped text-center" id="dataTable2">
                                <thead>
                                    <tr>
                                        <th>Kategori</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($kategori as $val)
                                    <tr>
                                        <td>{{ $val->kategori }}</td>
                                        <td>
                                            <a href="#" class="badge badge-warning edit-kategori" data-toggle="modal" data-target="#modal-kategori" data-url="{{ route('admin.setting.edit_kategori', Crypt::encrypt($val->id)) }}" data-value="{{ $val->kategori }}"><i class="fa fa-pen"></i> Edit</a>
                                            <a href="{{ route('admin.setting.hapus_kategori', Crypt::encrypt($val->id)) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                            <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card text-white mt-4">
                        <form action="{{ route('admin.setting.store_alamat') }}" method="post">
                            @csrf
                            <div class="card-header bg-primary py-1 px-3">
                                <small>Alamat</small>
                                <div class="float-right">
                                    <button type="submit" class="badge badge-primary border-0"><i class="fa fa-pen"></i> Simpan</button>
                                </div>
                            </div>
                            <div class="card-body bg-white">
                                <div class="editor">
                                    <textarea class="form-control" id="alamat" name="alamat">{{ $alamat->alamat }}</textarea>
                                </div>
                                <small class="text-dark">Masukan alamat dan click tombol simpan untuk mengubah</small>
                            </div>
                        </form>
                        <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                            <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                        </div>
                    </div>

                    <!-- Sosmed -->
                    <div class="card text-white mt-4">
                        <div class="card-header bg-primary py-1 px-3">
                            <small>Link Sosial media</small>
                        </div>
                        <div class="card-body bg-white">
                            <form action="{{ route('admin.setting.store_sosmed') }}" method="post">
                                @csrf
                                <small class="text-dark">Facebook</small>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary" type="button"><i class="fab fa-facebook"></i></button>
                                    </div>
                                    <input class="form-control" type="text" name="facebook" placeholder="..." value="@if(isset($sosmed->facebook)){{ $sosmed->facebook }}@endif" aria-label="sosmed" aria-describedby="basic-addon2" />
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-warning" type="button"><i class="fas fa-pen"></i></button>
                                    </div>
                                </div>
                            </form>

                            <form action="{{ route('admin.setting.store_sosmed') }}" method="post">
                                @csrf
                                <small class="text-dark">Twitter</small>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary" type="button"><i class="fab fa-twitter"></i></button>
                                    </div>
                                    <input class="form-control" type="text" name="twitter" placeholder="..." value="@if(isset($sosmed->twitter)){{ $sosmed->twitter }}@endif" aria-label="sosmed" aria-describedby="basic-addon2" />
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-warning" type="button"><i class="fas fa-pen"></i></button>
                                    </div>
                                </div>
                            </form>

                            <form action="{{ route('admin.setting.store_sosmed') }}" method="post">
                                @csrf
                                <small class="text-dark">Instagram</small>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary" type="button"><i class="fab fa-instagram"></i></button>
                                    </div>
                                    <input class="form-control" type="text" name="instagram" placeholder="..." value="@if(isset($sosmed->instagram)){{ $sosmed->instagram }}@endif" aria-label="sosmed" aria-describedby="basic-addon2" />
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-warning" type="button"><i class="fas fa-pen"></i></button>
                                    </div>
                                </div>
                            </form>

                            <form action="{{ route('admin.setting.store_sosmed') }}" method="post">
                                @csrf
                                <small class="text-dark">Youtube</small>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary" type="button"><i class="fab fa-youtube"></i></button>
                                    </div>
                                    <input class="form-control" type="text" name="youtube" placeholder="..." value="@if(isset($sosmed->youtube)){{ $sosmed->youtube }}@endif" aria-label="sosmed" aria-describedby="basic-addon2" />
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-warning" type="button"><i class="fas fa-pen"></i></button>
                                    </div>
                                </div>

                                <small class="text-dark">Masukan link dan click icon pen untuk mengubah</small>
                            </form>
                        </div>
                        <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                            <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal fade" id="modal-kategori">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title-kategori small"><strong>Tambah Kategori</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <small>Kategori</small>
                        <input type="text" class="form-control" name="kategori" id="kategori" placeholder="...">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn border" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary btn-simpan">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('myjs')
<script>
    $(document).ready(function() {
        $('.insert-kategori').on('click', function() {
            $('#modal-kategori .title-kategori').html('Tambah Kategori');
            $('#modal-kategori .btn-simpan').removeClass('bg-warning');
            $('#modal-kategori .btn-simpan').addClass('bg-primary');
            $('#modal-kategori .btn-simpan').removeClass('text-dark');
            $('#modal-kategori .btn-simpan').addClass('text-light');
            $('#modal-kategori form').attr('action', '<?= route('admin.setting.store_kategori') ?>');
        });
        $('.edit-kategori').on('click', function() {
            var url = $(this).data('url');
            var value = $(this).data('value');

            $('#modal-kategori .title-kategori').html('Edit Kategori');
            $('#modal-kategori .btn-simpan').removeClass('bg-primary');
            $('#modal-kategori .btn-simpan').addClass('bg-warning');
            $('#modal-kategori .btn-simpan').addClass('text-dark');
            $('#modal-kategori .btn-simpan').addClass('border-0');
            $('#modal-kategori form input[name=kategori]').val(value);
            $('#modal-kategori form').attr('action', url);
        });
    });
</script>
@endsection