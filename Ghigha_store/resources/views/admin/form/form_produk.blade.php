@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pt-4">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="card shadow-lg">
                <div class="card-header bg-primary small text-white"></div>
                <div class="card-body">
                    <form action="{{ route('admin.form.produk.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-img form-group text-center">
                            <div class="border rounded text-center img-canvas border-0">
                                <img src="" alt="" class="img-output">
                            </div>
                            <input type="file" name="thumbnail" id="imageUpload" style="display: none;" class="form-control">
                            <label class="btn juna-my-button mb-4" for="imageUpload" aria-describedby="inputGroupFileAddon02" style="margin-top: 2rem;">Choose File</label>
                        </div>
                        <div class="form-group">
                            <small>Nama</small>
                            <input type="text" name="nama" id="nama" class="form-control" required placeholder="...">
                        </div>
                        <div class="form-group">
                            <div class="container-fluid px-0">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <small>Harga</small>
                                        <input type="number" name="harga" id="harga" class="form-control" required placeholder="...">
                                    </div>
                                    <div class="col-lg-6">
                                        <small>Diskon</small>
                                        <input type="number" name="diskon" id="diskon" class="form-control" placeholder="...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <small>Kategori</small>
                            <select name="kategori_id" id="kategori_id" class="form-control">
                                @foreach($kategori as $val)
                                <option value="{{ $val->id }}">{{ $val->kategori }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <small>Promosi</small>
                            <textarea name="promosi" id="promosi" cols="30" rows="3" class="form-control" required placeholder="..."></textarea>
                        </div>
                        <div class="form-group">
                            <small>Deskripsi</small>
                            <textarea name="deskripsi" id="ckeditor" cols="30" rows="3" class="ckeditor" required placeholder="..."></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection