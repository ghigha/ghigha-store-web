@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pt-4">
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="card shadow-lg">
                <div class="card-header bg-primary small text-white"></div>
                <div class="card-body">
                    <form action="{{ route('admin.form.img_more.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-img form-group text-center">
                            <div class="border rounded text-center img-canvas border-0">
                                <img src="" alt="" class="img-output" height="100%">
                            </div>
                            <input type="file" name="thumbnail" id="imageUpload" style="display:none" class="form-control">
                            <label class="btn juna-my-button mb-4" for="imageUpload" aria-describedby="inputGroupFileAddon02" style="margin-top: 2rem;">Choose File</label>
                        </div>
                        <input type="hidden" name="produk_id" value="{{ $produk_id }}">
                        <div class="form-group text-center">
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection