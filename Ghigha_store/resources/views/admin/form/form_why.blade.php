@extends('layouts.admin_template')
@section('content')
<div class="container" style="padding-top: 2rem;">
    <small>Kenapa sih harus di <b>Ghigha Store</b> ?</small>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-primary"></div>
                <div class="card-body">
                    <form action="{{ route('admin.form.why.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <textarea name="kelebihan" id="ckeditor" class="ckeditor">@if(isset($kelebihan)){!! $kelebihan->kelebihan !!}@endif</textarea>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection