@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pt-4 mb-4">
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="card shadow-lg">
                <div class="card-header bg-primary small text-white">{{ $form_title }}</div>
                <div class="card-body">
                    <form action="{{ route('admin.form.store', Crypt::encrypt($page)) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-img form-group text-center">
                            <?php if (!isset($video)) { ?>
                                <div class="border rounded text-center img-canvas border-0">
                                    <img src="" alt="" class="img-output">
                                </div>
                            <?php } ?>
                            <input type="file" name="thumbnail" id="imageUpload" style="display: <?php if (isset($video)) { ?> block <?php } else { ?> none <?php } ?>;" class="form-control">
                            <?php if (!isset($video)) { ?>
                                <label class="btn juna-my-button mb-4" for="imageUpload" aria-describedby="inputGroupFileAddon02" style="margin-top: 2rem;">Choose File</label>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            @if($page == 1)
                            <small class="float-left title-url"></small>
                            <a href="#" class="badge badge-warning rounded float-right mb-3" data-toggle="modal" data-target="#modal-url">URL Produk</a>
                            @endif
                            <input type="text" name="title" id="title" class="form-control" <?php if ($page == 1) echo 'readonly' ?> required placeholder="@if($page != 1) Title @else URL @endif">
                        </div>
                        @if($deskripsi == 'on')
                        <div class="form-group">
                            <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Masukan Deskripsi"></textarea>
                        </div>
                        @endif
                        <div class="form-group text-right">
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<!-- url -->
<div class="modal fade" id="modal-url">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title small"><strong>Tambah url</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <table class="table table-striped table-content-middle">
                    <thead>
                        <tr>
                            <th>Thumbnail</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($produk as $val)
                        <tr>
                            <td><img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data"></td>
                            <td>{{ $val->nama }}</td>
                            <td><a href="#" onclick="setURLToInput()" data-link="<?= route('detail', Illuminate\Support\Facades\Crypt::encrypt($val->id)) ?>" data-title="{{ $val->nama }}" class="badge badge-primary btn-add-url" data-dismiss="modal"><i class="fa fa-plus"></i> Tambah</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer justify-content-between bg-primary">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection