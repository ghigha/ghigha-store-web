@extends('layouts.admin_template')
@section('content')
<div class="container-fluid pb-4 banner">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Testimoni</h1>
            <small>Percaya mereka yang telah membeli produk kami</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary py-1 px-3">
                    <small>Beri judul jika ada</small>
                    <div class="float-right">
                        <a href="{{ route('admin.form', Crypt::encrypt('4')) }}" class="badge badge-primary"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                </div>
                <div class="card-body bg-white table-responsive px-0 pt-0">
                    <table class="table table-striped text-center table-content-middle" id="dataTable2">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($testimoni as $val)
                            <tr>
                                <td>
                                    <img src="{{ asset($val->thumbnail) }}" alt="thumbnail" class="img-banner-data">
                                </td>
                                <td>{{ $val->title }}</td>
                                <td><a href="{{ route('admin.form.delete', [Crypt::encrypt('4'), Crypt::encrypt($val->id)]) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Video produk -->
<div class="container-fluid pb-4 banner" style="background-color: #F5F5F5">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Video Produk</h1>
            <small>Ghigha Store</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary py-1 px-3">
                    <small>Beri judul jika ada</small>
                    <div class="float-right">
                        <a href="{{ route('admin.form', Crypt::encrypt('5')) }}" class="badge badge-primary"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                </div>
                <div class="card-body bg-white table-responsive px-0 pt-0">
                    <table class="table table-striped text-center table-content-middle" id="dataTable2">
                        <thead>
                            <tr>
                                <th>Video</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($video as $val)
                            <tr>
                                <td><video src="{{ asset($val->thumbnail) }}" class="img-banner-data" controls></video></td>
                                <td>{{ $val->title }}</td>
                                <td><a href="{{ route('admin.form.delete', [Crypt::encrypt('5'), Crypt::encrypt($val->id)]) }}" class="badge badge-danger"><i class="fa fa-trash"></i> Hapus</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Kelabihan Ghigha Store -->
<div class="container-fluid pb-4 kelebihan">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="mt-4 mb-0">Kenapa Sih Harus di <b class="font-weight-bold">Ghigha Store</b> ?</h1>
            <small>Ghigha Store</small>
            <div class="card text-white mt-4">
                <div class="card-header bg-primary py-1 px-3">
                    <div class="float-right">
                        <a href="{{ route('admin.form.why') }}" class="badge badge-primary"><i class="fa fa-pen"></i> Edit</a>
                    </div>
                </div>
                <div class="card-body bg-white text-black">
                    <div class="row">
                        {!! $kelebihan->kelebihan !!}
                    </div>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between bg-primary">
                    <!-- <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection