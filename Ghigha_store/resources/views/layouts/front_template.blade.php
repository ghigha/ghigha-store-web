<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="Ghigha Store | Online Shop">
    <meta name="description" content="Online Shop Mudah, Aman dan Nyaman">
    <meta name="keywords" content="Ghigha Store, sepatu, online shop, brand indonesia">
    <meta name="author" content="Ghigha Store | Online Shop">
    <meta name="og:url" content="{{ url('') }}">
    <meta name="og:type" content="Website">
    <meta name="og:title" content="Ghigha Store | Online Shop">
    <meta name="og:description" content="Ghigha Store | Online Shop">
    <meta name="theme-color" content="white">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Facebook Pixel Code -->
    <script src="{{ asset('assets/js/facebook_fixel_code.js') }}"></script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1541308869383286&ev=PageView&noscript=1" />
    </noscript>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158071795-1"></script>
    <script src="{{ asset('assets/js/gtag.js') }}"></script>

    <!-- icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/logo_blank.png') }}">
    <!-- <link rel="icon" href="{{ asset('assets/images/logo.png') }}" sizes="200x200" type="image/icon"> -->

    <!-- Style -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('') }}assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/fonts/brand/style.css">

    <!--  -->
    <!-- <link rel="stylesheet" href="{{ asset('') }}/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="{{ asset('') }}assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/owl.theme.default.min.css">

    <!-- SweetAlert -->
    <link rel="stylesheet" href="{{ asset('') }}assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/plugins/toastr/toastr.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('') }}assets/css/style.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/junastyle.css">
    <!-- Font awesome -->
    <link rel="stylesheet" href="{{ asset('') }}assets/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/themify-icons.css">

</head>

<body>
    <!-- Kebutuhan -->
    <input type="hidden" name="url" id="url" value="{{ url('') }}">
    <input type="hidden" name="asset" id="asset" value="{{ asset('') }}">
    <!--  -->

    <!-- Nav -->
    <div class="position-fixed w-100" style="z-index: 9;">
        @if($status_nav == 'on')
        <!-- <header class="bg-light small text-black py-1 px-4 juna-header">
            <div class="float-right juna-header-right">
                <ul>
                    <li><a href="#">Cara Pembelian</a></li>
                    <li><a href="{{ route('login') }}">Masuk</a></li>
                </ul>
            </div>
            <div class="font-weight-bold">
                Online Shop
            </div>
        </header> -->
        @endif

        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white py-0 px-4 shadow-sm">
            <a href="{{ route('home') }}" class="navbar-brand @if($status_nav == 'off') w-100 text-center @endif">
                <img src="{{ asset('assets/images/logo.png') }}" alt="Brand" width="42" height="42" class="d-inline-block align-top">
                <strong class="h2 font-weight-bold text-black"><b> Ghigha Store </b></strong>
            </a>
            <!-- Navigation horizontal -->
            @if($status_nav == 'on')
            <div class="collapse navbar-collapse text-uppercase text-black juna-navbar" id="navbarNav">
                <ul>
                    <li class="active">
                        <a href="#">Home</a>
                    </li>
                    <!-- <li data-target="#subnavKategori" data-name="#Kategori" id="Kategori">
                        <a href="#">Kategori</a>
                    </li> -->
                </ul>
            </div>

            <div class="form-inline">
                <!-- btn toggle sidebar -->
                <div class="juna-btn-sidebar" onclick="toggleSidebar()">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- Search -->
                <div class="juna-nav-search">
                    <a href="#" class="text-black" style="cursor: pointer;"><span class="icon"><i class="fa fa-search"></i></span></a>
                    <input type="text" name="search" id="search" placeholder="Masukan kata kunci">
                </div>
            </div>
            @endif
        </nav>
        <!-- End Nav -->

        <!-- Sub nav -->
        <div class="juna-subnav bg-light" id="subnavKategori">
            <div class="container">
                <b class="font-weight-bold">Kategori</b>
                <ul class="mt-3" style="display: inline-block;">
                    @foreach(App\Kategori::all() as $val)
                    <li style="display:inline; padding-right: 10px"><a href="{{ route('produk', $val->kategori) }}" class="text-black">{{ $val->kategori }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- Sub nav -->

        <!-- Sidebar for tablet and hanphone -->
        <div id="juna-sidebar" class="text-uppercase">
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li>
                    <a href="#" onclick="toggleSubSidebar()">Kategori</a>
                    <span class="float-right"><i class="fa fa-angle-right"></i></span>
                </li>
            </ul>
        </div>
        <div id="juna-subsidebar" class="text-uppercase bg-light">
            <ul>
                <li><a href="#" class="font-weight-bold">Kategori</a></li>
                @foreach(App\Kategori::all() as $val)
                <li><a href="{{ route('produk', $val->kategori) }}" class="text-black">{{ $val->kategori }}</a></li>
                @endforeach
            </ul>
        </div>
        <!-- End Sidebar -->
    </div>
    <!-- End fixed content -->

    <!-- Content -->
    <div class="juna-content">
        @yield('content')
    </div>
    <!-- End Content -->

    <!-- Modal -->
    <div class="modal fade" id="modal-img-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="{{ asset('assets/images/Produk_2.JPG') }}" alt="image" class="img-modal">
                </div>
                <div class="modal-footer">
                    <div class="img-more row justify-content-center">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-video-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center video">
                    <video class="w-100" controls>
                        <source src="" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="bg-black">
        @if($status_nav == 'on')
        <div class="juna-container px-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 col-lg-2 px-0">
                        <div class="footer-title">
                            Kategori
                        </div>
                        <ul>
                            @foreach(App\Kategori::all() as $val)
                            <li><a href="#">{{ $val->kategori }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- <div class="col-sm-2 col-lg-2 px-0">
                        <div class="footer-title">
                            Bantuan
                        </div>
                        <ul>
                            <li><a href="#">Cara Pembelian</a></li>
                            <li><a href="#">Hub. +{{ App\No_telepon::first()->no_telepon }}</a></li>
                        </ul>
                    </div> -->
                    <div class="col-sm-2 col-lg-2 px-0">
                        <div class="footer-title">
                            Alamat
                        </div>
                        <ul>
                            <li><a href="#">{!! App\Alamat::first()->alamat !!}</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-8 col-lg-8 text-right pt-4">
                        <div class="social-icon">
                            <a href="{{ App\Sosmed::first()->facebook }}"><i class="ti-facebook"></i></a>
                            <a href="{{ App\Sosmed::first()->twitter }}"><i class="ti-twitter-alt"></i></a>
                            <a href="{{ App\Sosmed::first()->instagram }}"><i class="ti-instagram"></i></a>
                            <a href="{{ App\Sosmed::first()->youtube }}"><i class="ti-youtube"></i></a>
                            <a href="@if(isset($bantuan)){{ $bantuan }}@endif"><i class="fab fa-whatsapp"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="text-center">
            <div class="border-top py-1 px-2 text-white">
                <small>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy; Ghigha Store <script>
                        document.write(new Date().getFullYear());
                    </script> Developer By <a href="http://junmyportofolio.celahdeveloper.my.id"> Ujun Junaedi </a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </small>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <script src="{{ asset('assets/js/orderWA.js') }}"></script>
    <!-- Histats.com  START  (aync)-->
    <script type="text/javascript">
        var _Hasync = _Hasync || [];
        _Hasync.push(['Histats.start', '1,4435582,4,0,0,0,00010000']);
        _Hasync.push(['Histats.fasi', '1']);
        _Hasync.push(['Histats.track_hits', '']);
        (function() {
            var hs = document.createElement('script');
            hs.type = 'text/javascript';
            hs.async = true;
            hs.src = ('//s10.histats.com/js15_as.js');
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
        })();
    </script>
    <noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4435582&101" alt="" border="0"></a></noscript>
    <!-- Histats.com  END  -->

    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.animateNumber.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ asset('') }}assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/toastr/toastr.min.js"></script>

    <!-- <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script> -->
    <script src="{{ asset('assets/js/junajs.js') }}"></script>
    @yield('myjs')


</body>

</html>