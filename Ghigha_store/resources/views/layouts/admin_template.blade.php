<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="Ghigha Store | Online Shop">
    <meta name="description" content="Ghigha Store | Online Shop">
    <meta name="keywords" content="Ghigha Store, sepatu, online shop, brand indonesia">
    <meta name="author" content="Ghigha Store">
    <meta name="og:url" content="{{ url('') }}">
    <meta name="og:type" content="Website">
    <meta name="og:title" content="Ghigha Store">
    <meta name="og:description" content="Ghigha Store | Online Shop">
    <meta name="theme-color" content="white">
    <title>Admin</title>

    <!-- icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/logo_blank.png') }}">
    <!-- <link rel="icon" href="{{ asset('assets/images/logo.png') }}" sizes="200x200" type="image/icon"> -->

    <link href="{{ asset('') }}admin_assets/dist/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <!-- SweetAlert -->
    <link rel="stylesheet" href="{{ asset('') }}assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/junastyle.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/owl.theme.default.min.css">

</head>

<body class="sb-nav-fixed">

    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('/admin') }}">Ghigha Store</a>
        <a class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></a>
        <!-- Navbar-->
        <ul class="navbar-nav d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <!-- <div class="dropdown-divider"></div> -->
                    <a class="dropdown-item logout" href="{{ route('logout') }}">
                        Logout
                    </a>

                    <form id="formlogout" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Core</div>
                        <a class="nav-link" href="{{ url('/admin') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>
                        <div class="sb-sidenav-menu-heading">Landing Page</div>
                        <a class="nav-link" href="{{ route('admin.home') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Home
                        </a>
                        <a class="nav-link" href="{{ route('admin.detail_produk') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Detail Produk
                        </a>
                        <div class="sb-sidenav-menu-heading">Master</div>
                        <a class="nav-link" href="{{ route('admin.produk') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-archive"></i></div>
                            Produk
                        </a>
                        <a class="nav-link" href="{{ route('admin.setting') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-cog"></i></div>
                            Setting
                        </a>
                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logged in as:</div>
                    <small> Awesome Website </small>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                @yield('content')
            </main>

            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <small>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy; Ghigha Store <script>
                                document.write(new Date().getFullYear());
                            </script> Developer By <a href="http://junmyportofolio.celahdeveloper.my.id"> Ujun Junaedi </a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </small>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    @yield('modal')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('') }}admin_assets/dist/js/scripts.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('') }}admin_assets/dist/assets/demo/datatables-demo.js"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

    <!-- Sweet Alert -->
    <script src="{{ asset('') }}assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/toastr/toastr.min.js"></script>

    <!-- Ckeditor -->
    <script src="{{ asset('') }}admin_assets/ckeditor/ckeditor.js"></script>
    @yield('myjs')

    <!-- Sweet Alert 2 -->
    @if(session('success'))
    <script type="text/javascript">
        $(function() {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000
            });

            Toast.fire({
                type: 'info',
                icon: 'success',
                title: '{{ session("success") }}'
            });
        });
    </script>
    @endif

    <script>
        $(document).ready(function() {

            $('.logout').click(function() {
                event.preventDefault();
                Swal.fire({
                    title: 'Keluar Dari {{env("APP_NAME")}} ?',
                    showCancelButton: true,
                    confirmButtonText: 'Keluar',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        $('#formlogout').submit();
                    }
                });
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {

                        $('.img-output').css('opacity', '1');
                        $('.img-output').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imageUpload").change(function() {
                readURL(this);
            });

            $('#imageUpload').on('change', function() {
                let fileName = $(this).val().split('\\').pop();
            });

            clickImgMore();
            $('.video .owl-carousel').owlCarousel({
                loop: false,
                items: 2,
                margin: 10,
                merge: false,
                nav: true,
                dots: false,
                center: true,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 2,
                    },
                    1000: {
                        items: 2,
                    }
                }
            });

            $('.owl-carousel .owl-prev').html('<i class="fa fa-angle-left"></i>');
            $('.owl-carousel .owl-next').html('<i class="fa fa-angle-right"></i>');

            $('.testimonial .owl-carousel').owlCarousel({
                loop: true,
                items: 3,
                margin: 25,
                nav: true,
                dots: false,
                center: true,
                autoplay: true,
                timeout: 3000,
                autoplaySpeed: 500,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2,
                        margin: 5
                    },
                    600: {
                        items: 3,
                        margin: 10,
                    },
                    1000: {
                        items: 3,
                        margin: 25,
                    }
                }
            });
            var owl = $('.produk-detail-page .owl-carousel').owlCarousel({
                loop: false,
                items: 6,
                margin: 0,
                nav: true,
                dots: false,
                center: false,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 4
                    },
                    1000: {
                        items: 6
                    }
                }
            });
            // End slider
            function clickImgMore() {
                $('.img-produk-small img').on('click', function() {
                    var source = $(this).attr('src');
                    $('#img-first').attr('src', source);
                });
            }
        });

        $('.btn-add-url').on('click', function() {
            var dataUrl = $(this).data('link');
            var dataName = $(this).data('title');
            $('.title-url').html(dataName);
            $('#title').val(dataUrl);
        });

        $(function() {
            $('.banner #dataTable2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "responsive": false,
            });

            $('.terpopuler #dataTable2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "responsive": false,
            });

            $('.galeri #dataTable2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "responsive": false,
            });
            $('.produk #dataTable2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "responsive": false,
            });
        });
    </script>
</body>

</html>