@extends('layouts.front_template')
@section('content')
<div class="owl-carousel owl-theme juna-produk-image-slider">
    @foreach($banner as $val)
    <div class="item">
        <a href="@if($val->title != '') {{$val->title}} @else {{'#'}} @endif"><img src="{{ asset($val->thumbnail) }}" alt="Iklan"></a>
    </div>
    @endforeach
</div>
<!-- Best Produk -->
<section class="bg-white">
    <div class="juna-container">
        <h3>Terpopuler</h3>
        <div class="produk">
            <div class="container-fluid">
                <div class="row">
                    @foreach($populer as $val)
                    <div class="col-12 col-sm-6 col-lg-4 p-0 m-0">
                        <div class="produk-image" style="background-image: url(<?= asset($val->produk->thumbnail) ?>); cursor:pointer;">
                            <a href="{{ route('detail', Crypt::encrypt($val->produk->id)) }}">
                                <h4>{{ $val->produk->nama }}</h4>
                                <h6><small>Nyaman untuk di pakai</small></h6>
                                <div class="overlay-produk"></div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ghigha Store Produk Saller -->
<section class="bg-white">
    <div class="juna-container pt-2 ">
        <h3>Produk</h3>
        <div class="produk">
            <div class="container-fluid juna-seller">
                <div class="row">
                    @foreach($produk as $val)
                    <div class="col-6 col-lg-3 p-1">
                        <div class="produk-image-seller-produk">
                            <div class="produk-image-seller">
                                <img src="{{ asset($val->thumbnail) }}" alt="Produk" data-toggle="modal" data-target="#modal-img-detail" data-id="{{ $val->id }}">
                            </div>
                            <a href="{{ route('detail', Crypt::encrypt($val->id)) }}">
                                <h5 class="pb-0 mb-0">{{ $val->nama }}</h5>
                                <small class="coret-harga">Rp {{ number_format($val->harga, 0, ',', '.') }}</small>
                                <h6 class="p-0 m-0">Rp {{ number_format(round($val->harga - ($val->diskon/100*$val->harga)), 0, ',', '.') }}</h6>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center juna-container">
                    <a href="{{ route('produk') }}" class="juna-btn">Lebih Banyak <i class="fa fa-angle-right pl-2"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Galeri -->
<section class="bg-black">
    <div class="juna-container p-0">
        <div class="produk-galeri">
            <div class="container-fluid">
                <div class="row">
                    @foreach($galeri as $val)
                    <div class="col-6 col-sm-6 col-lg-3 p-0">
                        <div class="produk-image">
                            <img src="{{ asset($val->thumbnail) }}" alt="Galeri">
                            <h5>
                                <span class="font-weight-bold">{{ $val->title }}</span><br>
                                <small>{{ $val->deskripsi }}</small>
                            </h5>
                            <div class="overlay"></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Btn bantuan -->
<a href="{{ $bantuan }}" class="juna-btn-small bg-white shadow-sm text-black border" style="position: fixed; bottom:20px; right:20px; z-index:1; border-radius: 15px !important; border-bottom-right-radius: 0px !important">Butuh Bantuan? <i class="fab fa-whatsapp"></i></a>

@endsection
@section('myjs')
<script src="{{ asset('assets/js/homejs.js') }}"></script>
@endsection