@extends('layouts.front_template')
@section('content')
<section>
    <div class="juna-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-4 col-lg-3 py-0 pr-3">
                    <div class="list-kategori" style="padding-top: 25px">
                        <h6 class="text-black font-weight-bold pt-2">Kategori</h6>
                        <ul class="text-black">
                            @foreach($kategori as $val)
                            <li data-id="{{ $val->id }}">{{ $val->kategori }}</li>
                            @endforeach
                            <li data-id="all">Semua</li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-sm-8 col-lg-9">
                    <div class="produk">
                        <h6 class="text-black font-weight-bold">
                            Produk
                        </h6>
                        <small class="title-kategori">@if($title_kategori == "") Semua @else {{ $title_kategori }} @endif</small>
                        <div class="container-fluid pt-4 content-produk">
                            <div class="row">
                                @foreach($produk as $val)
                                <div class="col-6 col-lg-3 p-1">
                                    <div class="produk-image-seller-produk">
                                        <div class="produk-image-seller">
                                            <img src="{{ asset($val->thumbnail) }}" alt="Produk" data-toggle="modal" data-target="#modal-img-detail">
                                        </div>
                                        <a href="{{ route('detail', Crypt::encrypt($val->id)) }}">
                                            <h5 class="pb-0 mb-0">{{ $val->nama }}</h5>
                                            <small class="coret-harga">Rp {{ number_format($val->harga, 0, ',', '.') }}</small>
                                            <h6 class="p-0 m-0">Rp {{ number_format(round($val->harga - ($val->diskon/100*$val->harga)), 0, ',', '.') }}</h6>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('myjs')
<script src="{{ asset('assets/js/front_produk.js') }}"></script>
@endsection