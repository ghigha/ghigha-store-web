@extends('layouts.front_template')
@section('content')

<section class="juna-detail">
    <div class="banner-diskon">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-6 p-0">
                    <h1 class="text-white pb-0 mb-0">PENAWARAN TERBATAS! </h1>
                    <small class="pt-0">Penjualan akan ditutup setelah hitung mundur menunjukan 00 jam : 00 menit : 00 detik</small>
                    <!-- <h1 class="text-white pb-0 mb-0">Promo akan segera berakhir </h1>
                    <small class="pt-0">AYO Ambil promonya sekarang juga</small> -->
                </div>
                <div class="col-12 col-lg-6 p-0">
                    <div class="juna-countdown">
                        <span id="jam">00 <small>Jam</small></span> <strong class="font-weight-bold text-white"> : </strong>
                        <span id="menit">00 <small>Menit</small></span> <strong class="font-weight-bold text-white"> : </strong>
                        <span id="detik">00 <small>Detik</small></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="promo bg-light" style="padding:1rem">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="h5 text-black font-weight-bold">{{ $produk->promosi }}</h2>
                    <small class="text-black">GARANSI 100% GANTI BARU JIKA PRODUK TIDAK SESUAI GAMBAR</small>
                </div>
            </div>
        </div>
    </div>
    <div class="juna-container produk-detail-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-6 px-0">
                    <img src="{{ asset($produk->thumbnail) }}" alt="Produk" id="img-first" data-toggle="modal" data-target="#modal-img-detail">
                    <div class="owl-carousel owl-theme img-produk-small">
                        <div class="item" id="item-parent">
                            <img src="{{ asset($produk->thumbnail) }}" alt="Produk-more">
                        </div>
                        @foreach($img_more as $val)
                        <div class="item">
                            <img src="{{ asset($val->thumbnail) }}" alt="Produk-more">
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="produk-detail">
                        <small>{{ $produk->kategori->kategori }}</small>
                        <h1 id="productname">{{ $produk->nama }}</h1>
                        <h6>Nyaman untuk dipakai</h6>
                        <div class="progress progress-xs">
                            <div class="progress-bar gradient-orange text-uppercase" style="width: <?= $stok_prog ?>%">{{ $stok }} Tersisa</div>
                        </div>
                        <div class="mt-4">
                            <span class="badge badge-danger">Diskon {{ $produk->diskon }}%</span>
                            <h6 class="coret-harga mb-0">Rp {{ number_format($produk->harga, 0, ',', '.') }}</h6>
                            <h1 class="text-success" id="productprice" data-price="{{ round($produk->harga - ($produk->diskon/100*$produk->harga)) }}">Rp {{ number_format(round($produk->harga - ($produk->diskon/100*$produk->harga)), 0, ',', '.') }}</h1>
                        </div>
                        <div class="my-4 pt-2">
                            <span id="orderwa" data-url="{{ $order }}" class="juna-btn-lg"> <i class="fab fa-whatsapp h5 text-light"></i> Pesan Sekarang </span>
                        </div>
                        <div class="deskripsi mt-4 pt-4">
                            <div class="font-weight-bold mb-3">
                                Deskripsi
                            </div>
                            <p>
                                {!! $produk->deskripsi !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="text-center testimonial bg-light">
    <div class="juna-container px-1">
        <div class="h3 text-black" style="font-family: sans-serif;">
            <small>TESTIMONI</small>
        </div>
        <div class="h5">
            <small>Percaya pada mereka yang sudah membeli produk kami</small>
        </div>
        <div class="owl-carousel owl-theme">
            @foreach($testimoni as $val)
            <div class="item">
                <img src="{{ asset($val->thumbnail) }}" alt="Produk-more" data-toggle="modal" data-target="#modal-img-detail" style="height: 100%; width:auto; margin-left:50%; transform:translate(-50%, 0);">
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- Paking produk -->
<section class="video" data-jml="{{ $video->count() }}">
    <div class="juna-container text-center">
        <div class="h3 text-black" style="font-family: sans-serif;">
            <small>VIDEO PRODUK</small>
        </div>
        <div class="h5">
            <small>Ghigha Store</small>
        </div>
        <div class="owl-carousel owl-theme" style="margin-top: 3rem; margin-bottom: 3rem">
            @foreach($video as $val)
            <div class="item">
                <video class="w-100" controls>
                    <source src="{{ asset($val->thumbnail) }}" type="video/mp4">
                </video>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- Ket -->
<section class="why bg-light">
    <div class="juna-container text-center">
        <div class="h3 text-black" style="font-family: sans-serif;">
            <small>KENAPA SIH HARUS DI <b class="font-weight-bold">GHIGHA STORE</b> ?</small>
        </div>
        <div class="h5">
            <small>Bedanya dengan toko lain ?</small>
        </div>
        <div class="container-fluid pt-4 mt-4 text-black text-left">
            <div class="row">
                {!! $kelebihan->kelebihan !!}
            </div>
        </div>
    </div>
</section>

<section class="view-wa">
    <div class="juna-container text-center">
        <img src="{{ asset('assets/images/logo.png') }}" alt="logo" width="100px" height="100px">
        <h2 class="font-weight-bold text-black h1">Pengen tapi bayarnya dirumah aja ?</h2>
        <h6>Order Via WhatsApp aja, bayarnya setelah datang</h6>
        <div style="margin-top: 3rem;">
            <span id="orderwa1" class="juna-btn-lg"> <i class="fab fa-whatsapp h5 text-light"></i> Pesan Sekarang </span>
        </div>
    </div>
</section>

<section class="policy bg-light">
    <div class="juna-container text-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <img src="{{ asset('assets/images/policy.png') }}" alt="logo" class="img-policy" style="border-radius: 5px;">
                </div>
                <div class="col-12 col-lg-6">
                    <div class="mt-2">
                        <h2 class="text-black h1"><i class="fa fa-ban text-danger"></i> N.B. : Hati hati barang tiruan, yang asli hanya di <i class="font-weight-bold">Ghigha Store</i></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Button fixed bottom -->
<span id="orderwa2" class="juna-btn-small" style="position: fixed; bottom:20px; right:20px; z-index:1"> <i class="fab fa-whatsapp text-light"></i> Pesan Sekarang </span>

@endsection
@section('myjs')
<script src="{{ asset('assets/js/detail_frontjs.js') }}"></script>
<script src="{{ asset('assets/js/popup.js') }}"></script>
@endsection