<?php

namespace App;

use Carbon\Carbon;
use App\Cache_key;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    protected $table = 'video';
    protected $fillable = ['thumbnail', 'title'];

    public static function getall($key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () {
            return Video::all();
        });
    }
}
