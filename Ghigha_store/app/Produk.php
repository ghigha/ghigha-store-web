<?php

namespace App;

use Carbon\Carbon;
use App\Cache_key;
use App\Kategori;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    //
    protected $table = 'produk';
    protected $fillable = ['kategori_id', 'thumbnail', 'nama', 'harga', 'diskon', 'stok', 'promosi', 'deskripsi'];

    public static function getall($key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () {
            return Produk::orderBy('id', 'desc')->get();
        });
    }

    public static function getSearch($key, $search)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () use ($search) {
            $kategori = Kategori::where('kategori', $search)->first();
            if ($kategori != []) {
                return Produk::where('kategori_id', $kategori->id)->orderBy('id', 'desc')->get();
            } else {
                return Produk::where('nama', 'like', '%' . $search . '%')->orderBy('id', 'desc')->get();
            }
        });
    }


    public static function findproduk($id, $key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () use ($id) {
            return Produk::find($id);
        });
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function produk()
    {
        return $this->hasOne('App\Populer_produk');
    }
}
