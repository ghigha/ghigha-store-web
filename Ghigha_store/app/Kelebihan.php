<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelebihan extends Model
{
    //
    protected $table = 'kelebihan';
    protected $fillable = ['kelebihan'];
}
