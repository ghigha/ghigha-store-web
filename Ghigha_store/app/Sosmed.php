<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sosmed extends Model
{
    //
    protected $table = 'sosmed';
    protected $fillable = ['facebook', 'twitter', 'instagram', 'youtube'];
}
