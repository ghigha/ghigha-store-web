<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Cache_key;

class Img_more extends Model
{
    //
    protected $table = 'img_more';
    protected $fillabel = ['produk_id', 'thumbnail'];

    public static function getall($key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () {
            return Img_more::all();
        });
    }

    public static function getByIdProduk($key, $produk_id)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () use ($produk_id) {
            return Img_more::where('produk_id', $produk_id)->get();
        });
    }
}
