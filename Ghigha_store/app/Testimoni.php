<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Cache_key;

class Testimoni extends Model
{
    //
    protected $table = 'testimoni';
    protected $fillable = ['thumbnail', 'title'];

    public static function getall($key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () {
            return Testimoni::all();
        });
    }
}
