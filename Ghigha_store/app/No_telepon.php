<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class No_telepon extends Model
{
    //
    protected $table = 'no_telepon';
    protected $fillable = ['no_telepon'];
}
