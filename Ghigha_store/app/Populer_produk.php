<?php

namespace App;

use Carbon\Carbon;
use App\Cache_key;
use Illuminate\Database\Eloquent\Model;

class Populer_produk extends Model
{
    //
    protected $table = 'populer_produk';
    protected $fillable = ['produk_id'];

    public static function getall($key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () {
            return Populer_produk::orderBy('id', 'desc')->get();
        });
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
