<?php

namespace App\Http\Controllers\Front;

use App\Banner;
use App\Detail_ukuran;
use App\Galeri;
use App\Http\Controllers\Controller;
use App\Img_more;
use App\Kategori;
use App\Kelebihan;
use App\No_telepon;
use App\Populer_produk;
use App\Produk;
use App\Testimoni;
use App\Video;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;

class FrontController extends Controller
{
    //
    public function index()
    {
        $data['title'] = 'Home';
        $data['status_nav'] = 'on';
        $data['banner'] = Banner::getall('banner');
        $data['populer'] = Populer_produk::getall('populer');
        $data['produk'] = Produk::getall('produk');
        $data['galeri'] = Galeri::getall('galeri');
        $data['bantuan'] = "https://api.whatsapp.com/send?phone=" . No_telepon::first()->no_telepon . "&text=Hai%20Kak,%20Saya%20butuh%20bantuan";
        return view('front.front_home', $data);
    }

    public function detailProduk($id)
    {
        Artisan::call('cache:clear');
        try {
            $id = Crypt::decrypt($id);
        } catch (DecryptException $msg) {
            $id = $id;
        }

        $produk = Produk::findproduk($id, 'produk_detail');
        if ($produk->stok > 1) {
            $stok = $produk->stok - 1;
        } else {
            $stok = 99;
        }

        $update = Produk::find($produk->id);
        $update->update(['stok' => $stok]);

        $data['title'] = 'Detail';
        $data['status_nav'] = 'off';
        $data['produk'] = $produk;
        $data['img_more'] = Img_more::getall('img_more');
        $data['stok'] = $stok;
        $data['stok_prog'] = ($stok / 99) * 100;
        $data['detail_ukuran'] = Detail_ukuran::first();
        $data['testimoni'] = Testimoni::getall('testimoni_front');
        $data['video'] = Video::getall('Video_front');
        $data['no_video'] = 1;
        $data['order'] = "https://api.whatsapp.com/send?phone=" . No_telepon::first()->no_telepon . "&text=Hai%20kak,%20apakah%20produk%20" . $produk->nama . "%20Masih%20ada?%0ASaya%20tertarik";
        // Produk%3A%20" . route('detail', Crypt::encrypt($id)) . "%0AInfo%20Produk%0AGambar%3A%20" . asset($produk->thumbnail) . "%0ANama%3A%20" . $produk->nama
        $data['kelebihan'] = Kelebihan::first();
        return view('front.front_detail', $data);
    }

    public function produk($search = null)
    {
        Artisan::call("cache:clear");
        if ($search != null) {
            $produk = Produk::getSearch('produk_search', $search);
        } else {
            $produk = Produk::getall('produk');
        }

        $data['title'] = 'Produk';
        $data['status_nav'] = 'on';
        $data['produk'] = $produk;
        $data['title_kategori'] = $search;
        $data['kategori'] = Kategori::all();
        $data['bantuan'] = "https://api.whatsapp.com/send?phone=" . No_telepon::first()->no_telepon . "&text=Hai%20Kak,%20Saya%20butuh%20bantuan";
        return view('front.front_produk', $data);
    }
}
