<?php

namespace App\Http\Controllers\Admin;

use App\Alamat;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Facades\App\Cache_key;
use App\Banner;
use App\Detail_ukuran;
use App\Galeri;
use App\Kategori;
use App\Kelebihan;
use App\No_telepon;
use App\Populer_produk;
use App\Produk;
use App\Sosmed;
use App\Testimoni;
use App\Video;

class AdminController extends Controller
{
    //
    public function index()
    {
        $data['produk'] = Produk::getall('produk');
        return view('admin.index', $data);
    }

    public function home()
    {
        $data['banner'] = Banner::getall('benner');
        $data['populer'] = Populer_produk::getall('populer');
        $data['terpopuler'] = Produk::getall('produk');
        $data['galeri'] = Galeri::getall('galeri');
        return view('admin.home', $data);
    }

    public function detail_produk()
    {
        $data['testimoni'] = Testimoni::getall('testimoni');
        $data['video'] = Video::getall('video');
        $data['kelebihan'] = Kelebihan::first();
        return view('admin.detail', $data);
    }

    public function produk()
    {
        $data['produk'] = Produk::getall('produk');
        return view('admin.produk', $data);
    }

    public function setting()
    {
        $data['no_hp'] = No_telepon::first();
        $data['alamat'] = Alamat::first();
        $data['detail_ukuran'] = Detail_ukuran::first();
        $data['kategori'] = Kategori::all();
        $data['sosmed'] = Sosmed::first();
        return view('admin.setting', $data);
    }
}
