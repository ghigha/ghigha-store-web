<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Galeri;
use App\Http\Controllers\Controller;
use App\Kelebihan;
use App\Populer_produk;
use App\Produk;
use App\Testimoni;
use App\Video;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;

class FormController extends Controller
{
    // Catatan page nomber
    // 1 banner         4 testimoni
    // 2 terpopuler     5 video produk
    // 3 galeri

    private function cekPage($page)
    {
        if ($page == '1') {
            $form_title = 'Banner';
            $thumbnail = 'on';
            $title = 'on';
            $deskripsi = 'off';
            $path = 'storage/thumbnails_banner';
        } else if ($page == '2') {
            $form_title = 'Terpopuler';
            $thumbnail = 'off';
            $title = 'off';
            $deskripsi = 'off';
            $path = '';
        } else if ($page == '3') {
            $form_title = 'Galeri';
            $thumbnail = 'on';
            $title = 'on';
            $deskripsi = 'on';
            $path = 'storage/thumbnails_galeri';
        } else if ($page == '4') {
            $form_title = 'Testimoni';
            $thumbnail = 'on';
            $title = 'on';
            $deskripsi = 'off';
            $path = 'storage/thumbnails_testimoni';
        } else {
            $form_title = 'Video';
            $thumbnail = 'off';
            $title = 'off';
            $deskripsi = 'off';
            $data['video'] = 'on';
            $path = 'storage/video';
        }

        $data['form_title'] = $form_title;
        $data['thumbnail'] = $thumbnail;
        $data['title'] = $title;
        $data['deskripsi'] = $deskripsi;
        $data['path'] = $path;

        return $data;
    }

    public function index($page)
    {
        $page = Crypt::decrypt($page);

        $data['page'] = $page;

        $cekPage = $this->cekPage($page);

        $data['form_title'] = $cekPage['form_title'];
        $data['thumbnail'] = $cekPage['thumbnail'];
        $data['title'] = $cekPage['title'];
        $data['deskripsi'] = $cekPage['deskripsi'];
        $data['produk'] = Produk::all();
        if (isset($cekPage['video'])) {
            $data['video'] = $cekPage['video'];
        }

        return view('admin.form.index', $data);
    }

    public function store(Request $request, $page)
    {
        $page = Crypt::decrypt($page);

        $cekPage = $this->cekPage($page);

        if ($request->file() != null) {
            $file = $request->file('thumbnail');

            $tujuan_upload = $cekPage['path'];
            $file->move($tujuan_upload, time() . $file->getClientOriginalName());
            $file = $tujuan_upload . '/' . time() . $file->getClientOriginalName();
        } else {
            $file = '';
        }



        $obj['thumbnail'] = $file;
        $obj['title'] = $request->title;
        if (isset($request->deskripsi)) {
            $obj['deskripsi'] = $request->deskripsi;
        }

        $this->insertToTable($page, $obj);
        // Clear cahce 
        Artisan::call('cache:clear');
        if ($page <= 3) {
            return redirect()->route('admin.home')->with('success', 'Berhasil tambah data');
        } else {
            return redirect()->route('admin.detail_produk')->with('success', 'Berhasil tambah data');
        }
    }

    public function edit($page, $id)
    {
        $page = Crypt::decrypt($page);
        $id = Crypt::decrypt($id);
        $cekPage = $this->cekPage($page);
        if ($page == 1) {
            $update = Banner::find($id);
        } else if ($page == 3) {
            $update = Galeri::find($id);
        }

        $data['form_title'] = $cekPage['form_title'];
        $data['thumbnail'] = $cekPage['thumbnail'];
        $data['title'] = $cekPage['title'];
        $data['deskripsi'] = $cekPage['deskripsi'];
        $data['produk'] = Produk::all();
        if (isset($cekPage['video'])) {
            $data['video'] = $cekPage['video'];
        }
        $data['data'] = $update;
        $data['page'] = $page;

        return view('admin.form.form_edit', $data);
    }

    public function update(Request $request, $page, $id)
    {
        $page = Crypt::decrypt($page);
        $id = Crypt::decrypt($id);
        if ($page == 1) {
            $update = Banner::find($id);
        } else if ($page == 3) {
            $update = Galeri::find($id);
        }

        $cekPage = $this->cekPage($page);

        if ($request->file() != null) {
            $file = $request->file('thumbnail');

            $tujuan_upload = $cekPage['path'];
            $file->move($tujuan_upload, time() . $file->getClientOriginalName());
            $file = $tujuan_upload . '/' . time() . $file->getClientOriginalName();
            $obj['thumbnail'] = $file;
            File::delete($update->thumbnail);
        }

        $obj['title'] = $request->title;
        if (isset($request->request)) {
            $obj['deskripsi'] = $request->deskripsi;
        }

        $update->update($obj);
        Artisan::call('cache:clear');
        if ($page <= 3) {
            return redirect()->route('admin.home')->with('success', 'Berhasil update data');
        } else {
            return redirect()->route('admin.detail_produk')->with('success', 'Berhasil update data');
        }
    }

    public function delete($page, $id)
    {
        $page = Crypt::decrypt($page);
        $id = Crypt::decrypt($id);

        if ($page == '1') {
            $delete = Banner::find($id);
        } else if ($page == '2') {
            $delete = '';
        } else if ($page == '3') {
            $delete = Galeri::find($id);
        } else if ($page == '4') {
            $delete = Testimoni::find($id);
        } else {
            $delete = Video::find($id);
        }

        File::delete($delete->thumbnail);
        $delete->delete();
        // Clear cahce 
        Artisan::call('cache:clear');
        if ($page <= 3) {
            return redirect()->route('admin.home')->with('success', 'Berhasil di hapus');
        } else {
            return redirect()->route('admin.detail_produk')->with('success', 'Berhasil di hapus');
        }
    }

    private function insertToTable($page, $obj)
    {
        if ($page == '1') {
            Banner::insert($obj);
        } else if ($page == '2') {
            $form_title = 'Terpopuler';
            $thumbnail = 'off';
            $title = 'off';
            $deskripsi = 'off';
        } else if ($page == '3') {
            Galeri::insert($obj);
        } else if ($page == '4') {
            Testimoni::insert($obj);
        } else {
            Video::insert($obj);
        }
    }

    public function form_why()
    {
        $data['kelebihan'] = Kelebihan::first();
        return view('admin.form.form_why', $data);
    }

    public function store_kelebihan(Request $request)
    {
        $kelebihan = Kelebihan::first();

        $obj['kelebihan'] = $request->kelebihan;

        if ($kelebihan == []) {
            Kelebihan::insert($obj);
            $msg = 'Berhasil insert kelebihan';
        } else {
            $kelebihan->update($obj);
            $msg = 'Berhasil update kelebihan';
        }
        return redirect()->route('admin.detail_produk')->with('success', $msg);
    }
}
