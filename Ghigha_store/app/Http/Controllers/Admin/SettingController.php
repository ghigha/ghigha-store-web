<?php

namespace App\Http\Controllers\Admin;

use App\Alamat;
use App\Detail_ukuran;
use App\Http\Controllers\Controller;
use App\Kategori;
use App\No_telepon;
use App\Sosmed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class SettingController extends Controller
{
    //
    public function store_nohp(Request $request)
    {
        $no_hp = No_telepon::first();
        if ($no_hp == []) {
            No_telepon::insert($request->except(['_token']));
        } else {
            $no_hp->update($request->except(['_token']));
        }
        return redirect()->route('admin.setting')->with('success', 'Berhasil update data');
    }

    public function store_kategori(Request $request)
    {
        Kategori::insert($request->except(['_token']));
        return redirect()->route('admin.setting')->with('success', 'Berhasil tambah kategori');;
    }

    public function edit_kategori(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        $kategori = Kategori::find($id);
        $kategori->update($request->except(['_token']));
        return redirect()->route('admin.setting')->with('success', 'Berhasil update kategori');
    }

    public function hapus_kategori($id)
    {
        $id = Crypt::decrypt($id);
        $kategori = Kategori::find($id);
        $kategori->delete();

        return redirect()->route('admin.setting')->with('success', 'Berhasil hapus data');;
    }

    public function store_alamat(Request $request)
    {
        $alamat = Alamat::first();
        if ($alamat == []) {
            Alamat::insert($request->except(['_token']));
        } else {
            $alamat->update($request->except(['_token']));
        }

        return redirect()->route('admin.setting')->with('success', 'Berhasil update data');;
    }

    // Sosmed
    public function store_sosmed(Request $request)
    {
        $sosmed = Sosmed::first();
        if ($sosmed == []) {
            if (isset($request->facebook)) {
                $facebook = $request->facebook;
            } else {
                $facebook = '';
            }
            if (isset($request->twitter)) {
                $twitter = $request->twitter;
            } else {
                $twitter = '';
            }
            if (isset($request->instagram)) {
                $instagram = $request->instagram;
            } else {
                $instagram = '';
            }
            if (isset($request->youtube)) {
                $youtube = $request->youtube;
            } else {
                $youtube = '';
            }

            $obj['facebook'] = $facebook;
            $obj['twitter'] = $twitter;
            $obj['instagram'] = $instagram;
            $obj['youtube'] = $youtube;
            Sosmed::insert($obj);
        } else {
            $sosmed->update($request->except(['_token']));
        }

        return redirect()->route('admin.setting')->with('success', 'Berhasil update link');
    }
}
