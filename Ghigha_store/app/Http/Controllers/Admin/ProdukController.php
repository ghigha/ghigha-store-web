<?php

namespace App\Http\Controllers\Admin;

use App\Detail_ukuran;
use App\Http\Controllers\Controller;
use App\Img_more;
use App\Kategori;
use App\Populer_produk;
use App\Produk;
use App\Testimoni;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;

class ProdukController extends Controller
{
    //
    public function index()
    {
        $data['kategori'] = Kategori::all();
        return view('admin.form.form_produk', $data);
    }

    public function getProduk($kategori_id)
    {
        if ($kategori_id == 'all') {
            $produk = Produk::all();
        } else {
            $produk = Produk::where('kategori_id', $kategori_id)->get();
        }

        echo json_encode($produk);
    }

    public function getImageMore($produk_id)
    {
        $img_more = Img_more::where('produk_id', $produk_id)->get();
        echo json_encode($img_more);
    }

    public function detail($id)
    {
        $id = Crypt::decrypt($id);
        $data['produk'] = Produk::find($id);
        $data['detail_ukuran'] = Detail_ukuran::first();
        $data['img_more'] = Img_more::getByIdProduk('img_more', $id);
        $data['testimoni'] = Testimoni::getall('testimoni');
        $data['video'] = Video::getall('Video_front');
        $data['no_video'] = 1;
        return view('admin.detail_produk', $data);
    }

    public function form_edit($id)
    {
        $id = Crypt::decrypt($id);
        $data['produk'] = Produk::find($id);
        $data['kategori'] = Kategori::all();
        return view('admin.form.form_edit_produk', $data);
    }

    public function store_populer($produk_id)
    {
        $produk_id = Crypt::decrypt($produk_id);
        Populer_produk::insert(['produk_id' => $produk_id]);
        Artisan::call('cache:clear');
        return redirect()->route('admin.home')->with('success', 'Berhasil tambah populer');
    }

    public function delete_populer($id)
    {
        $id = Crypt::decrypt($id);
        $populer = Populer_produk::find($id);
        $populer->delete();
        Artisan::call('cache:clear');
        return redirect()->route('admin.home')->with('success', 'Berhasil hapus populer');
    }

    public function store(Request $request)
    {
        if ($request->file() != null) {
            $file = $request->file('thumbnail');

            $tujuan_upload = 'storage/thumbnail_produk';
            $file->move($tujuan_upload, time() . $file->getClientOriginalName());
            $file = $tujuan_upload . '/' . time() . $file->getClientOriginalName();
        } else {
            $file = '';
        }



        $obj['thumbnail'] = $file;
        $obj['nama'] = $request->nama;
        $obj['harga'] = $request->harga;
        $obj['diskon'] = $request->diskon;
        $obj['kategori_id'] = $request->kategori_id;
        $obj['promosi'] = $request->promosi;
        $obj['deskripsi'] = $request->deskripsi;
        Artisan::call('cache:clear');
        Produk::insert($obj);
        return redirect()->route('admin.produk')->with('success', 'Berhasil insert data');
    }

    public function proses_edit(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        $produk = Produk::find($id);

        if ($request->file() != null) {
            File::delete($produk->thumbnail);
            $file = $request->file('thumbnail');
            $tujuan_upload = 'storage/thumbnail_produk';
            $file->move($tujuan_upload, time() . $file->getClientOriginalName());
            $file = $tujuan_upload . '/' . time() . $file->getClientOriginalName();
            $obj['thumbnail'] = $file;
        }

        $obj['nama'] = $request->nama;
        $obj['harga'] = $request->harga;
        $obj['diskon'] = $request->diskon;
        $obj['kategori_id'] = $request->kategori_id;
        $obj['promosi'] = $request->promosi;
        $obj['deskripsi'] = $request->deskripsi;
        Artisan::call('cache:clear');
        $produk->update($obj);
        return redirect()->route('admin.produk.detail', Crypt::encrypt($id))->with('Success', 'Berhasil update data');
    }


    public function delete($id)
    {
        $id = Crypt::decrypt($id);
        $produk = Produk::find($id);
        File::delete($produk->thumbnail);
        Artisan::call('cache:clear');
        $produk->delete();
        return redirect()->route('admin.produk')->with('Success', 'Berhasil hapus data');
    }

    public function img_more($produk_id)
    {
        $produk_id = Crypt::decrypt($produk_id);
        $data['produk_id'] = $produk_id;
        return view('admin.form.form_img_more', $data);
    }

    public function store_img_more(Request $request)
    {
        if ($request->file() != null) {
            $file = $request->file('thumbnail');

            $tujuan_upload = 'storage/thumbnails_img_more';
            $file->move($tujuan_upload, time() . $file->getClientOriginalName());
            $file = $tujuan_upload . '/' . time() . $file->getClientOriginalName();
        } else {
            $file = '';
        }

        $obj['produk_id'] = $request->produk_id;
        $obj['thumbnail'] = $file;

        Img_more::insert($obj);
        Artisan::call('cache:clear');
        return redirect()->route('admin.produk.detail', Crypt::encrypt($request->produk_id))->with('Success', 'Berhasil tambah image');
    }

    public function del_img_more($id, $produk_id)
    {
        $id = Crypt::decrypt($id);
        $produk_id = Crypt::decrypt($produk_id);
        $img_more = Img_more::find($id);
        $img_more->delete();
        Artisan::call('cache:clear');
        return redirect()->route('admin.produk.detail', Crypt::encrypt($produk_id))->with('Success', 'Berhasil hapus image');
    }
}
