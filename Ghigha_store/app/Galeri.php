<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Cache_key;

class Galeri extends Model
{
    //
    protected $table = 'galeri';
    protected $fillable = ['thumbnail', 'title', 'deskripsi'];

    public static function getall($key)
    {
        return cache()->remember(Cache_key::getCacheKey(Cache_key::key($key)), Carbon::now()->addMinute(5), function () {
            return Galeri::all();
        });
    }
}
