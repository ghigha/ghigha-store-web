<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cache_key extends Model
{
    //
    const CACHE_KEY = 'KEYDATA';

    public static function key($key)
    {
        $key = "all.{$key}";
        return $key;
    }

    public static function getCacheKey($key)
    {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }
}
