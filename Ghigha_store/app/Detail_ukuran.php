<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_ukuran extends Model
{
    //
    protected $table = 'detail_ukuran';
    protected $fillable = ['detail_ukuran'];
}
