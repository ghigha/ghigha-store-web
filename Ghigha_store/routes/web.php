<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (isset(Auth::user()->name)) {
        return redirect()->route('admin.home');
    } else {
        return redirect()->route('home');
    }
});

Auth::routes();

Route::get('/home', 'Front\FrontController@index')->name('home');
Route::get('/detail/{id}', 'Front\FrontController@detailProduk')->name('detail');
Route::get('/produk/{search?}', 'Front\FrontController@produk')->name('produk');

// get produk by kategori
Route::get('/produk/get_produk/{kategori_id}', 'Admin\ProdukController@getProduk');
Route::get('/getImageMore/{produk_id}', 'Admin\ProdukController@getImageMore')->name('getImageMore');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', 'Admin\AdminController@index');
    Route::name('admin.')->group(function () {
        Route::get('/admin/home', 'Admin\AdminController@home')->name('home');
        Route::get('/admin/detail_produk', 'Admin\AdminController@detail_produk')->name('detail_produk');
        Route::get('/admin/produk', 'Admin\AdminController@produk')->name('produk');
        Route::get('/admin/setting', 'Admin\AdminController@setting')->name('setting');

        // Img More
        Route::get('/admin/form/img_more/{produk_id}', 'Admin\ProdukController@img_more')->name('form.img_more');
        Route::post('/admin/form/img_more/store', 'Admin\ProdukController@store_img_more')->name('form.img_more.store');
        Route::get('/admin/form/img_more/del/{id}/{produk_id}', 'Admin\ProdukController@del_img_more')->name('form.img_more.del');

        // Form
        // why
        Route::get('/admin/form/why', 'Admin\FormController@form_why')->name('form.why');
        Route::get('/admin/form/{page}', 'Admin\FormController@index')->name('form');
        Route::post('/admin/form/store/{page}', 'Admin\FormController@store')->name('form.store');
        Route::get('/admin/form/edit/{page}/{id}', 'Admin\FormController@edit')->name('form.edit');
        Route::post('/admin/form/update/{page}/{id}', 'Admin\FormController@update')->name('form.update');
        Route::post('/admin/form/why/store', 'Admin\FormController@store_kelebihan')->name('form.why.store');
        Route::get('/admin/form/delete/{page}/{id}', 'Admin\FormController@delete')->name('form.delete');

        // Populer produk
        Route::get('/admin/produk/populer/{id}', 'Admin\ProdukController@store_populer')->name('produk.populer');
        Route::get('/admin/produk/populer/delete/{id}', 'Admin\ProdukController@delete_populer')->name('produk.populer.delete');

        // Produk
        Route::get('/admin/produk/detail/{id}', 'Admin\ProdukController@detail')->name('produk.detail');
        Route::get('/admin/form/produk/form_produk', 'Admin\ProdukController@index')->name('form.produk.form_produk');
        Route::get('/admin/form/produk/form_edit/{id}', 'Admin\ProdukController@form_edit')->name('form.produk.form_edit');
        Route::post('/admin/form/produk/store', 'Admin\ProdukController@store')->name('form.produk.store');
        Route::post('/admin/form/produk/edit/{id}', 'Admin\ProdukController@proses_edit')->name('form.produk.edit');
        Route::get('/admin/form/produk/delete/{id}', 'Admin\ProdukController@delete')->name('form.produk.delete');


        // Setting
        Route::post('/admin/setting/store_nohp', 'Admin\SettingController@store_nohp')->name('setting.store_nohp');
        Route::post('/admin/setting/store_kategori', 'Admin\SettingController@store_kategori')->name('setting.store_kategori');
        Route::post('/admin/setting/edit_kategori/{id}', 'Admin\SettingController@edit_kategori')->name('setting.edit_kategori');
        Route::get('/admin/setting/hapus_kategori/{id}', 'Admin\SettingController@hapus_kategori')->name('setting.hapus_kategori');
        Route::post('/admin/setting/store_alamat', 'Admin\SettingController@store_alamat')->name('setting.store_alamat');
        Route::post('/admin/setting/store_sosmed', 'Admin\SettingController@store_sosmed')->name('setting.store_sosmed');
    });
});
