$(document).ready(function () {
    // Slider
    var owl = $('.owl-carousel').owlCarousel({
        loop: true,
        items: 1,
        margin: 0,
        merge: false,
        nav: false,
        dots: true,
        center: false,
        autoplay: true,
        autoplaySpeed: 500,
    });
    $('.owl-theme').append('<small class="text-black d-none">Kami sediakan barang terbaik untuk anda</small>');
    // End slider
});