var url = $('#url').val();
var asset = $('#asset').val();
function toggleSidebar() {
    $('#juna-subsidebar').removeClass('active');
    if ($('#juna-sidebar').hasClass('active')) {
        $('#juna-sidebar').removeClass('active');
    } else {
        $('#juna-sidebar').addClass('active');
    }
}

function toggleSubSidebar() {
    toggleSidebar();
    if ($('#juna-subsidebar').hasClass('active')) {
        $('#juna-subsidebar').removeClass('active');
    } else {
        $('#juna-subsidebar').addClass('active');
    }
}

$('.navbar ul li').hover(function () {
    var target = $(this).data('target');
    var name = $(this).data('name');
    $(target).css('display', 'block');
    $('.navbar ul li' + name).css('border-bottom', '2px solid #000000');
}, function () {
    var target = $(this).data('target');
    var name = $(this).data('name');

    $(target).hover(function () {
        $(target).css('display', 'block');
        $('.navbar ul li' + name).css('border-bottom', '2px solid #000000');
    }, function () {
        $(target).css('display', 'none');
        $('.navbar ul li' + name).css('border-bottom', '2px solid #ffffff');
    });

    $(target).css('display', 'none');
    $('.navbar ul li' + name).css('border-bottom', '2px solid #ffffff');
});

$('img').on('click', function () {
    var src = $(this).attr('src');
    var produk_id = $(this).data('id');
    $('#modal-img-detail .modal-content .modal-body img').attr('src', src);
    getImgMore(produk_id);
});

function getImgMore(produk_id) {
    $.ajax({
        url: url + '/getImageMore/' + produk_id,
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $('#modal-img-detail .modal-footer .img-more').empty();
            $.each(data, function (i, row) {
                var item = '<div class="col-4 col-sm-2">' +
                    '<img src="' + asset + row.thumbnail + '" alt="Produk-more" class="w-100" style="height:70px; width: 100px; cursor:pointer">' +
                    '</div>';
                $('#modal-img-detail .modal-footer .img-more').append(item);
            });

            img_modal_more();
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function img_modal_more() {
    $('#modal-img-detail .modal-footer .img-more img').on('click', function () {
        var src = $(this).attr('src');
        $('#modal-img-detail .modal-body img').attr('src', src);
    });
}

$('.img-produk-small img').on('click', function () {
    $('.img-produk-small img').css('opacity', '0.3');
    $(this).css('opacity', '1');
});

$('.juna-nav-search input[name=search]').on('keyup', function () {
    var search = $(this).val();
    $('.juna-nav-search a').attr('href', url + '/produk/' + search);
});

// $('.modal').on('hide.bs.modal', function (e) {
//     $("element.style").css("padding-right", "0");
// });

