$(document).ready(function () {
    setInterval(function () {
        const random = getRandomData();
        const time = getRandomTime();
        runSwal(random.name, random.product, time);
    }, 8000);



    const listOrang = ["Setiawan Suhendra", "Susanto Handoko", "Gotama Shirong", "Lauwita You", "Lesley Titarsole", "Moses Situmeang", "Mishael Bintang", "Haggai Wuruwu", "Raharjo", "Suratman", "Batari Wangi Kurnia", "Widyawati Gunardi", "Wongso Jiahui", "Fangestu Mingxia", "Doortje Pattinaja", "Eunice Sinabutar", "Joy Sitompul", "Hadassah Pane", "Dwi", "Tuminindyah", "Ari", "Ayu", "Tika", "Aulia", "Pratiwi", "Utami", "Anggi", "Tri", "Dwi", "Agus", "Lestari", "Maria", "Novita", "Tyas", "Nur", "Wahyu", "Ade", "Riza", "Arya", "Hadi", "Bayu", "Ria", "Dian", "Eka", "Indah", "Angga", "Rizal", "Mila", "Lia", "Yani", "Rina", "Adit", "Astuti", "Indra", "Yudi", "Adi", "Putra", "Nurul", "Yulia", "Anna", "Gita", "Andy", "Rian", "Andi", "Arif", "Annisa", "Rini", "Fajar", "Ilham", "Sari", "Sri", "Rizki", "Amalia", "Kusuma", "Rio", "Dinda", "Ika", "Yuni", "Maya", "Devi", "Widya", "Agung", "Andre", "Putri", "Dewi", "Wulan", "Ajie", "Budi", "Reza", "Yunita",];
    const product = [
        {
            name: "Flat Shoes Premium",
            img: asset + 'assets/images/Produk_1.JPG',
        },
        {
            name: "Sepatu Semi Formal Premium",
            img: asset + 'assets/images/Produk_2.JPG',
        },
        {
            name: "Sepatu Sendal Flat",
            img: asset + 'assets/images/Produk_3.JPG',
        },
        {
            name: "Sepatu Boots Wanita Kulit Heels Premium",
            img: asset + 'assets/images/Produk_4.JPG',
        }
    ];

    const getRandomData = () => {
        const idxOrang = (Math.floor(Math.random() * 20));
        const idxProduct = (Math.floor(Math.random() * 4));
        return {
            name: listOrang[idxOrang],
            product: product[idxProduct]
        };
    }

    const getRandomTime = () => {
        const time = Math.floor(Math.random() * 3600) + 1;
        const acak = Math.floor(Math.random() * 60) == 0 ? 1 : Math.floor(Math.random() * 60);
        const random = Math.floor(time / acak);
        const getHourOrMin = random > 60 ? `${Math.floor(random / 60)} Jam yang lalu` : `${random} Menit yang lalu`;
        return getHourOrMin;
    }

    const runSwal = (orang, product, time) => {
        Swal.fire({
            position: 'bottom-start',
            showConfirmButton: false,
            timer: 3000,
            showClass: {
                popup: 'animate__animated animate__fadeInUp'
            },
            html: '<div class="container-fluid pl-0">' +
                '<div class="row">' +
                '<div class="col-3 p-0">' +
                '<img src="' + product.img + '" alt="Img" height="100%" width="40px">' +
                '</div>' +
                '<div class="col-9 p-0 text-black text-left">' +
                '<div class="font-weight-bold" style="font-family: sans-serif">' +
                getRandomData().name + '<small> telah membeli ' + product.name + '</small>' +
                '</div>' +
                '<div class="text-right">' +
                '<small>' + getRandomTime() + '</small>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>',
            // hideClass: {
            //     popup: 'animate__animated animate__fadeInDown'
            // },
            toast: true,
            scrollbarPadding: false,
            customClass: {
                // container: "my-shadow",
                content: "hide-scroll"
            }
        });
    }

    console.log(asset);
});