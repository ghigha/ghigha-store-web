var asset = $('#asset').val();
$(document).ready(function () {
    clickImgMore();
    // Slider
    var owl = $('.produk-detail-page .owl-carousel').owlCarousel({
        loop: false,
        items: 6,
        margin: 0,
        nav: true,
        dots: false,
        center: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 3
            },
            600: {
                items: 4
            },
            1000: {
                items: 6
            }
        }
    });
    // End slider

    // Slider testimonial
    // Slider
    $('.testimonial .owl-carousel').owlCarousel({
        loop: true,
        items: 4,
        margin: 25,
        nav: true,
        dots: false,
        center: true,
        autoplay: true,
        timeout: 3000,
        autoplaySpeed: 500,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                margin: 5
            },
            600: {
                items: 3,
                margin: 10,
            },
            1000: {
                items: 4,
                margin: 25,
            }
        }
    });
    // End slider
    // Video slider
    $('.video .owl-carousel').owlCarousel({
        loop: false,
        items: 2,
        margin: 10,
        merge: false,
        nav: true,
        dots: false,
        center: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 2,
            }
        }
    });

    $('.owl-carousel .owl-prev').html('<i class="fa fa-angle-left"></i>');
    $('.owl-carousel .owl-next').html('<i class="fa fa-angle-right"></i>');

    countdown();
});

function countdown() {
    $('.juna-countdown').countdown('2021/12/3', function (event) {
        $('#jam').html(event.strftime('%H <small>Jam</small>'));
        $('#menit').html(event.strftime('%M <small>Menit</small>'));
        $('#detik').html(event.strftime('%S <small>Detik</small>'));
    });
}

function clickImgMore() {
    $('.img-produk-small img').on('click', function () {
        var source = $(this).attr('src');
        $('#img-first').attr('src', source);
    });
}