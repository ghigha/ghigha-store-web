var url = $('#url').val();
var asset = $('#asset').val();
$(document).ready(function () {
    $('.list-kategori ul li').on('click', function () {
        kategori = $(this).html();
        kategori_id = $(this).data('id');
        $('.title-kategori').html(kategori);

        getProdukKategori(kategori_id);
    });

    function getProdukKategori(kategori_id) {
        $.ajax({
            url: url + '/produk/get_produk/' + kategori_id,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $('.content-produk .row').empty();
                $.each(data, function (i, row) {
                    var diskon = row.harga - (row.diskon / 100 * row.harga);
                    var view = '<div class="col-6 col-lg-3 p-1">' +
                        '<a href="' + url + '/detail/' + row.id + '">' +
                        '<div class="produk-image-seller-produk">' +
                        '<div class="produk-image-seller">' +
                        '<img src="' + asset + row.thumbnail + '" alt="Produk">' +
                        '</div>' +
                        '<h5 class="pb-0 mb-0">' + row.nama + '</h5>' +
                        '<small class="coret-harga">Rp ' + number_format(row.harga) + '</small>' +
                        '<h6 class="p-0 m-0">Rp ' + number_format(diskon) + '</h6>' +
                        '</div>' +
                        '</a>' +
                        '</div>';
                    $('.content-produk .row').append(view);
                });
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function number_format(harga) {
        var bilangan = harga;

        var number_string = bilangan.toString(),
            sisa = number_string.length % 3,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah;
    }
});